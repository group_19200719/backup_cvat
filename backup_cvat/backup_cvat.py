import os
from glob import glob
from prefect import task


@task(log_prints=True)
def stop_cvat(cvat_dir):
    os.chdir(cvat_dir)
    print("Приостанавливаем CVAT docker-composed контейнеры")
    cvat_stop_cmd = (
        "docker-compose "
        "-f docker-compose.yml "
        "-f components/serverless/docker-compose.serverless.yml "
        "stop"
    )

    sys_code = os.system(cvat_stop_cmd)
    if sys_code:
        raise ValueError


@task(log_prints=True)
def restart_cvat(cvat_dir, your_ip_address=""):
    os.chdir(cvat_dir)
    print("Запускаем CVAT docker-composed контейнеры")

    if your_ip_address:
        cvat_export_host_cmd = f"export CVAT_HOST={your_ip_address}"

        sys_code = os.system(cvat_export_host_cmd)
        if sys_code:
            raise ValueError

    cvat_restart_cmd = (
        "docker-compose "
        "-f docker-compose.yml "
        "-f components/serverless/docker-compose.serverless.yml "
        "start"
    )

    sys_code = os.system(cvat_restart_cmd)
    if sys_code:
        raise ValueError


@task(log_prints=True)
def backup_cvat_db(backup_folder, cur_time):
    backup_dir, backup_folder = os.path.split(backup_folder)

    os.chdir(backup_dir)
    print("Создаю бекап БД CVAT")
    cvat_backup_cmd = (
        f"docker run --rm "
        f"--name temp_backup "
        f"--volumes-from cvat_db "
        f"-v $(pwd)/{backup_folder}:/{backup_folder} ubuntu "
        f"tar -czvf /{backup_folder}/{cur_time}_cvat_db.tar.gz "
        f"/var/lib/postgresql/data"
    )

    sys_code = os.system(cvat_backup_cmd)
    if sys_code:
        raise ValueError


@task(log_prints=True)
def backup_cvat_data(backup_folder, cur_time):
    backup_dir, backup_folder = os.path.split(backup_folder)

    os.chdir(backup_dir)
    print("Создаю бекап данных CVAT")
    cvat_backup_cmd = (
        f"docker run --rm "
        f"--name temp_backup "
        f"--volumes-from cvat_server "
        f"-v $(pwd)/{backup_folder}:/{backup_folder} ubuntu "
        f"tar -czvf /{backup_folder}/{cur_time}_cvat_data.tar.gz "
        f"/home/django/data"
    )

    sys_code = os.system(cvat_backup_cmd)
    if sys_code:
        raise ValueError


@task(log_prints=True)
def backup_cvat_data_to_folder(backup_folder, cur_time):
    backup_dir, backup_folder = os.path.split(backup_folder)

    os.chdir(backup_dir)
    print("Создаю бекап данных CVAT")
    cvat_backup_cmd = (
        f"docker run --rm "
        f"--name temp_backup "
        f"--volumes-from cvat_server "
        f"-v $(pwd)/{backup_folder}:/{backup_folder} ubuntu "
        f"cp -rva /home/django/data /{backup_folder}"
    )

    sys_code = os.system(cvat_backup_cmd)
    if sys_code:
        raise ValueError


@task(log_prints=True)
def backup_cvat_events(backup_folder, cur_time):
    backup_dir, backup_folder = os.path.split(backup_folder)

    os.chdir(backup_dir)
    print("Создаю бекап событий CVAT")
    cvat_backup_cmd = (
        f"docker run --rm "
        f"--name temp_backup "
        f"--volumes-from cvat_elasticsearch "
        f"-v $(pwd)/{backup_folder}:/{backup_folder} ubuntu "
        f"tar -czvf /{backup_folder}/cvat_events_{cur_time}.tar.gz "
        f"/usr/share/elasticsearch/data"
    )

    sys_code = os.system(cvat_backup_cmd)
    if sys_code:
        print("Бекап событий не создан!")


@task(log_prints=True)
def restore_cvat_db(path_to_backup_folder):
    os.chdir(path_to_backup_folder)
    cvat_db_name = glob("*cvat_db*")

    if cvat_db_name:
        cvat_db_name = cvat_db_name[0]
        print(f"Восстанавливаем БД CVAT из {cvat_db_name}")
        cvat_backup_cmd = (
            f"docker run --rm "
            f"--name temp_backup "
            f"--volumes-from cvat_db "
            f"-v $(pwd):/backup ubuntu bash "
            f"-c 'cd /var/lib/postgresql/data && tar -xvf /backup/{cvat_db_name} --strip 4'"
        )

        sys_code = os.system(cvat_backup_cmd)
        if sys_code:
            raise ValueError
    else:
        print(f"бекап БД CVAT в {path_to_backup_folder} не найден!")


@task(log_prints=True)
def restore_cvat_data(path_to_backup_folder):
    os.chdir(path_to_backup_folder)
    cvat_data_name = glob("*cvat_data*")

    if cvat_data_name:
        cvat_data_name = cvat_data_name[0]
        print(f"Восстанавливаем данные CVAT из {cvat_data_name}")
        cvat_backup_cmd = (
            f"docker run --rm "
            f"--name temp_backup "
            f"--volumes-from cvat_server "
            f"-v $(pwd):/backup ubuntu bash "
            f"-c 'cd /home/django/data && tar -xvf /backup/{cvat_data_name} --strip 3'"
        )

        sys_code = os.system(cvat_backup_cmd)
        if sys_code:
            raise ValueError
    else:
        print(f"бекап данных CVAT в {path_to_backup_folder} не найден!")


@task(log_prints=True)
def restore_cvat_data_from_folder(path_to_backup_folder):
    os.chdir(path_to_backup_folder)

    if os.path.exists('data'):
        print(f"Восстанавливаем данные CVAT...")
        cvat_backup_cmd = (
            f"docker run --rm "
            f"--name temp_backup "
            f"--volumes-from cvat_server "
            f"-v $(pwd):/backup ubuntu bash "
            f"-c 'cp -rva /backup/data/* /home/django/data'"
        )
        
        sys_code = os.system(cvat_backup_cmd)
        if sys_code:
            raise ValueError
    else:
        print(f"бекап данных CVAT в {path_to_backup_folder} не найден!")


@task(log_prints=True)
def restore_cvat_events(path_to_backup_folder):
    os.chdir(path_to_backup_folder)
    cvat_events_name = glob("*cvat_events*")

    if cvat_events_name:
        cvat_events_name = cvat_events_name[0]
        print(f"Восстанавливаем события CVAT из {cvat_events_name}")
        cvat_backup_cmd = (
            f"docker run --rm "
            f"--name temp_backup "
            f"--volumes-from cvat_elasticsearch "
            f"-v $(pwd):/backup ubuntu bash "
            f"-c 'cd /usr/share/elasticsearch/data && tar -xvf /backup/{cvat_events_name} --strip 4'"
        )

        sys_code = os.system(cvat_backup_cmd)
        if sys_code:
            raise ValueError
    else:
        print(f"бекап событий CVAT в {path_to_backup_folder} не найден!")
